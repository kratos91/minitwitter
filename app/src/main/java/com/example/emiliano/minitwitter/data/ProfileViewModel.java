package com.example.emiliano.minitwitter.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.emiliano.minitwitter.retrofit.request.RequestUserProfile;
import com.example.emiliano.minitwitter.retrofit.response.ResponseUserProfile;


public class ProfileViewModel extends AndroidViewModel {
    public ProfileRepository profileRepository;
    public LiveData<ResponseUserProfile> userProfile;
    public LiveData<String> photoUrl;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        profileRepository=new ProfileRepository();
        userProfile=profileRepository.getProfile();
        photoUrl=profileRepository.getPhotoProfile();
    }

    public void updatProfile(RequestUserProfile requestUserProfile){
        profileRepository.updateProfile(requestUserProfile);
    }

    public void uploadPhoto(String photo){
        profileRepository.uploadPhoto(photo);
    }
}
