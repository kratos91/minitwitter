package com.example.emiliano.minitwitter.data;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.example.emiliano.minitwitter.retrofit.response.Tweet;
import com.example.emiliano.minitwitter.ui.tweets.BottomModalTweetFragment;

import java.util.List;

public class TweetViewModel extends AndroidViewModel {
    private LiveData<List<Tweet>> tweets;
    private LiveData<List<Tweet>> favTweets;
    private TweetRepository tweetRepository;

    public TweetViewModel(@NonNull Application application) {
        super(application);
        tweetRepository=new TweetRepository();
    }

    public LiveData<List<Tweet>> getTweets(){
        tweets=tweetRepository.getAllTweets();
        return tweets;
    }

    public LiveData<List<Tweet>> getNewTweets(){
        tweets=tweetRepository.getAllTweets();
        return tweets;
    }

    public LiveData<List<Tweet>> getNewFavTweets(){
        getNewTweets();
        return getFavTweets();
    }

    public void insertTweet(String mensaje){
        tweetRepository.createTweet(mensaje);
    }

    public void likeTweet(int idTweet){
        tweetRepository.likeTweet(idTweet);
    }

    public LiveData<List<Tweet>> getFavTweets(){
        favTweets=tweetRepository.getFavsTweet();
        return favTweets;
    }

    public void deleteTweet(int idTweet){
        tweetRepository.deleteTweet(idTweet);
    }

    public void openDialogTweetMenu(Context ctx,int idTweet){
        BottomModalTweetFragment dialogTweet=BottomModalTweetFragment.newInstance(idTweet);
        dialogTweet.show(((AppCompatActivity)ctx).getSupportFragmentManager(),"BottomModalTweetFragment");
    }
}
