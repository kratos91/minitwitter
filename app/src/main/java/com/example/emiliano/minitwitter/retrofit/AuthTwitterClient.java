package com.example.emiliano.minitwitter.retrofit;

import com.example.emiliano.minitwitter.common.Constantes;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AuthTwitterClient {
    private static AuthTwitterClient instance=null;
    private AuthTwitterService miniTwitterService;
    private Retrofit retrofit;

    public AuthTwitterClient(){
        //Incluir en la cabecera de la peticion el token

        OkHttpClient.Builder okHttpClientBuilder=new OkHttpClient.Builder();
        okHttpClientBuilder.addInterceptor(new AuthInterceptor());
        OkHttpClient cliente=okHttpClientBuilder.build();


        retrofit=new Retrofit.Builder()
                .baseUrl(Constantes.API_MINITWITTER_BASE_URL)
                .client(cliente)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        miniTwitterService=retrofit.create(AuthTwitterService.class);
    }//fin de constructor

    //Patrón Singleton
    public static AuthTwitterClient getInstance(){
        if(instance==null){
            instance=new AuthTwitterClient();
        }
        return instance;
    }

    public AuthTwitterService getAuthTwitterService(){
        return miniTwitterService;
    }
}
