package com.example.emiliano.minitwitter.ui.tweets;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.emiliano.minitwitter.R;
import com.example.emiliano.minitwitter.common.Constantes;
import com.example.emiliano.minitwitter.data.TweetViewModel;
import com.example.emiliano.minitwitter.retrofit.response.Tweet;

import java.util.List;


public class TweetListFragment extends Fragment {
    RecyclerView recyclerView;
    MyTweetRecyclerViewAdapter adapter;
    List<Tweet> tweetList;
    TweetViewModel tweetViewModel;
    SwipeRefreshLayout swipeRefreshLayout;

    private int tweetListType = 1;


    public TweetListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static TweetListFragment newInstance(int tweetListType) {
        TweetListFragment fragment = new TweetListFragment();
        Bundle args = new Bundle();
        args.putInt(Constantes.TWEET_LIST_TYPE, tweetListType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tweetViewModel= ViewModelProviders.of(getActivity())
                .get(TweetViewModel.class);

        if (getArguments() != null) {
            tweetListType = getArguments().getInt(Constantes.TWEET_LIST_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweet_list, container, false);


        Context context = view.getContext();
        recyclerView= view.findViewById(R.id.list);

        swipeRefreshLayout= view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAzul));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                if(tweetListType==Constantes.TWEET_LIST_ALL){
                    loadNewData();
                }else if(tweetListType==Constantes.TWEET_LIST_FAV){
                    loadNewFavData();
                }
            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        adapter=new MyTweetRecyclerViewAdapter(
                getActivity(),
                tweetList
        );
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setRefreshing(true);

        if(tweetListType==Constantes.TWEET_LIST_ALL){
            loadTweetData();
        }else if(tweetListType==Constantes.TWEET_LIST_FAV){
            loadFavTweetData();
        }



        return view;
    }

    private void loadFavTweetData() {
        tweetViewModel.getFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList=tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
            }
        });
    }

    private void loadNewFavData() {
        tweetViewModel.getNewFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList=tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
                tweetViewModel.getNewFavTweets().removeObserver(this);
            }
        });
    }

    private void loadTweetData() {

        tweetViewModel.getTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList=tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
            }
        });

    }

    private void loadNewData() {

        tweetViewModel.getNewTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                tweetList=tweets;
                swipeRefreshLayout.setRefreshing(false);
                adapter.setData(tweetList);
                tweetViewModel.getNewTweets().removeObserver(this);
            }
        });

    }

}
